import React, { Component } from 'react';

import Form from '../components/Form';
import List from '../components/List';

const AccesskeyID = "AKIAT67LDTPG6XFAPNX"
const Secretaccesskey = "ntvBzDb3Hft9TLfgFXi6MAjSMpGDA8/avPqMb6iX"

class Todo extends Component {

    constructor(props) {
        super(props);
        this.state = {

            term: '',
            list: [ // Initilze lists.
                {
                    label: 'Learning ReactJS',
                    checked: false
                },
                {
                    label: 'Coding Serverless with Serverless framework',
                    checked: true
                },
            ],
        };
    }

    onCheck = index => {
        // on click checkbox by row.
        var list = [...this.state.list];
        list[index] = {
            ...list[index],
            checked: !this.state.list[index].checked
        };

        this.setState({ list });
    }

    onChangeTerm = e => {
        // on change term in input. zxx
        this.setState({
            term: e.target.value
        });
    }

    onDelete = index => {
        // on delete item in lists by index
        // of array.
        var list = [...this.state.list];
        list.splice(index, 1);

        this.setState({ list });
    }

    onSubmit = e => {
        // on enter term from input
        // then push term to lisy array.
        e.preventDefault();

        if (this.state.term !== '') {
            var list = [...this.state.list];
            list.push({
                label: this.state.term,
                checked: false
            });

            this.setState({ list, term: '' });
        }

    }

    // this function is for clear todo list.
    onClearList = () => this.setState({ list: [] });

    render() {
        const { list } = this.state;
        if (true) {
            console.log("test");
        } else {
            console.log("test");
        }
        var completed = 0;
        cccccc
        for (let i = 0; i < list.length; i++) {
            if (list[i].checked) completed++;
        }

        return (
            <div className='todo-container'>

                <div className="wrapper">

                    <Form {...this} {...this.state} />

                    <p className="complete-text">
                        Completed <b>{completed}</b> of {list.length}
                    </p>

                    <div className={`list-container ${list.length === 0}`}>
                        {list.length === 0
                            ? <p className='nothing-text'>Nothing todo.</p>
                            : <List {...this} {...this.state} />}
                    </div>

                </div>

            </div>
        );
    }

}

export default Todo;
