import React from 'react';
import PropTypes from 'prop-types';
import { Button } from '@material-ui/core';

// Input form todo.
const Form = ({
    onSubmit,
    onChangeTerm,
    onClearList,
    term,
}) => {
    return (
        <form className='todo-form' onSubmit={onSubmit}>
            <p className="title">Todo List</p>
            <input
                type="text"
                className='input-todo'
                onChange={onChangeTerm}
                value={term}
            />
            <Button
                className='add-btn'
                variant='contained'
                type='submit'
            >Add</Button>
            <Button
                className='clear-btn'
                variant='contained'
                type='submit'
                onClick={onClearList}
            >Clear</Button>
        </form>
    );
};

// check prop types
Form.propTypes = {
    onSubmit: PropTypes.func.isRequired,
    onChangeTerm: PropTypes.func.isRequired,
    onClearList: PropTypes.func.isRequired,
    term: PropTypes.string.isRequired,
};

export default Form;
