import React from 'react';
import PropTypes from 'prop-types';

import { Checkbox } from '@material-ui/core';

// Render todo list.
const List = ({ list, onCheck, onDelete }) => {
    return (
        <ul>{
            list.map((item, index) => {
                return (
                    <span className='list-row' key={index}>
                        <li onClick={() => onCheck(index)}>

                            <Checkbox checked={item.checked} />

                            <p className={`label ${item.checked}`}>
                                {item.label}
                            </p>

                        </li>

                        <div className="delete-container">
                            <i
                                className="fas fa-trash"
                                onClick={() => onDelete(index)}
                            />
                        </div>

                    </span>
                );
            })
        }</ul>
    );
};

// check prop types
List.propTypes = {
    list: PropTypes.arrayOf(PropTypes.object).isRequired,
    onDelete: PropTypes.func.isRequired
};

export default List;