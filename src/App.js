/**
 * Render app
 */
import React, { Component, Fragment } from 'react';

import './styles/Todo.css';
import TodoList from './pages/Todo';

class App extends Component {

    render() {
        return (
            <Fragment>

                <TodoList />

            </Fragment>
        );
    }

}

export default App;
